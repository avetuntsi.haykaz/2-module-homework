s1 = input('Первая строка: ')
s2 = input('Вторая строка: ')

if s1 == s2:
    print ('Строки идентичны')
elif len(s1) != len(s2):
    print ('Первую строку нельзя получить из второй с помощью циклического сдвига.')
else:
    k = 1
    transfer = False
    for i in range (len(s2)-1):
        s2 = s2[-1]+s2[:-1]
        if s2 == s1:
            transfer = True
            print ('Первая строка получается из второй со сдвигом ', k)
            break
        else:
            k += 1
    if not transfer:
        print ('Первую строку нельзя получить из второй с помощью циклического сдвига.')


