first_line = []
second_line = []

for height in range (160,176+1,2):
    first_line.append(height)

for height in range (162,180+1,3):
    second_line.append(height)

first_line.extend(second_line)

for number_min in range(len(first_line)):
    for current in range(number_min, len(first_line)):
        if first_line[current] < first_line[number_min]:
            first_line[current], first_line[number_min] = first_line[number_min], first_line[current]

print ('Отсортированный список учеников: ', first_line)
