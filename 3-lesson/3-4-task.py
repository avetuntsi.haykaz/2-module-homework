guests = ['Петя', 'Ваня', 'Саша', 'Лиза', 'Катя']

while True:
    print ('Сейчас на вечеринке', len(guests), 'человек:',guests )
    print ('Гость пришёл или ушёл?', end = ' ')
    answer = input ()
    if answer == 'пришёл':
        guest_name = input('Имя гостя: ')
        if len(guests) == 6:
            print ('Прости, ', guest_name , 'но мест нет.')
        else:
            guests.append(guest_name)
            print ('Привет, ', guest_name , '!')

    elif answer == 'ушёл':
        guest_name = input('Имя гостя: ')
        guests.remove(guest_name)
        print ('Пока, ', guest_name , '!')


    elif answer == 'Пора спать':
        print ('Вечеринка закончилась, все легли спать.')
        break


