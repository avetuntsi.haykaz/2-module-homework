print ('Введите координаты монетки:')
x_coordinat = float(input('x:'))
y_coordinat = float(input('y:'))
radius = float(input('Введите радиус: '))

moneta_point = (x_coordinat ** 2 + y_coordinat ** 2 ) ** 0.5

if moneta_point < radius:
    print ('Монетка где-то рядом')
else:
    print ('Монетки в области нет')