text = input('текст: ').lower()
sym_dict = dict()

for sym in text:
    if sym in sym_dict:
        sym_dict[sym] += 1
    else:
        sym_dict[sym] = 1

for i_sym in sorted(sym_dict.keys()):
    print (i_sym , ':', sym_dict[i_sym])

print ('Инвертированный словарь частот:')

text_dict = dict()

for i_letter, i_amount in sym_dict.items():
    if i_amount not in text_dict:
        text_dict[i_amount] = []
        text_dict[i_amount].append(i_letter )
    else:
        text_dict[i_amount].append(i_letter)

for i_text in text_dict:
    print (i_text, ':', text_dict[i_text])


# здесь что-то написано

