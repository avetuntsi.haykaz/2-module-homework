word = input ('Введите слово: ')
word_length = len(word)
word_list = list(word)

count = 0

for id in range (word_length-1):
    if word_list [id] == word_list [word_length - 1 - id]:
        count += 1
    else:
        count += 0

if count == word_length - 1:
    print ('Слово является палиндромом')
else:
    print ('Слово не является палиндромом')