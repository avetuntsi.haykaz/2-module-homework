letters = "аоиеёэыуюя"

text = input("Введите текст: ")
list = [letter for letter in text if letter in letters]
print("Список гласных букв:", list)
print("Длина списка:", len(list))