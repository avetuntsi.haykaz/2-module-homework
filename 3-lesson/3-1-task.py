one_list = [1, 5, 3]
second_list = [1, 5, 1, 5]
third_list = [1, 3, 1, 5, 3, 3]

one_list.extend(second_list)
count_5 = one_list.count(5)
print('Количество цифр 5 при первом объединении: ', count_5)
for _ in range(count_5):
    one_list.remove(5)

one_list.extend(third_list)
count_3 = one_list.count(3)
print('Количество цифр 3 при первом объединении: ', count_3)

print ('Итоговый список ', one_list)
