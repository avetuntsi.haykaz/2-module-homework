import random

first_team = []
second_team = []
winner = []

for x in range(20):
    first_team.append(round(random.uniform(5,10),2))
    second_team.append(round(random.uniform(5,10),2))

    if first_team[x] >= second_team[x]:
        winner.append(first_team[x])
    else:
        winner.append(second_team[x])

print('Первая команда: ', first_team)
print('Вторая команда:  ', second_team)
print('Победители тура: ', winner)